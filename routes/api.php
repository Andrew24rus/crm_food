<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$c = $app->getContainer();
$app->post('/api/login', 'UserController:doLogin');
$app->get('/api/logout', 'UserController:doLogout');

$app->post('/api/file', function (Request $request, Response $response) {
    $data = $request->getParams();
    $file = new File($_FILES['file'], $data);
});

$app->group('/api', function() use ($app) {
    $c = $app->getContainer();

    $app->post('/food/create', 'FoodController:createFood');
    $app->post('/food/delete', 'FoodController:deleteFood');

    $app->post('/user', 'UserController:createUser')->add(new Access($c, ['user_create']));
    $app->post('/users/roles/get_role', 'UserController:getRole')->add(new Access($c, ['user_view']));
    $app->post('/users/roles/create', 'UserController:createRole')->add(new Access($c, ['user_create']));
    $app->post('/user/{id}', 'UserController:editUser')->add(new Access($c, ['user_update']));
});