<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response) {
    return $this->view->render($response, 'layout.html.twig');
});
$app->get('/login', 'UserController:showLoginPage');

$app->get('/food', 'FoodController:showFood');