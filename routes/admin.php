<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$c = $app->getContainer();

$app->group('/admin', function() use($app) {
	$c = $app->getContainer();
});

$app->group('/admin', function() use($app) {

	$c = $app->getContainer();
	//Пользователи
	$app->get('/users', 'AdminController:showUserList')->add(new Access($c, ['user_view']));
	$app->get('/users/roles', 'AdminController:showUserRoles')->add(new Access($c, ['user_view', 'user_create']));
	$app->get('/user/{id}', 'AdminController:showUserEdit')->add(new Access($c, ['user_view', 'user_update']));
	$app->get('/user', 'AdminController:showUserAdd')->add(new Access($c, ['user_view', 'user_create']));

	$app->get('/settings', 'AdminController:showSettings')->add(new Access($c, ['settings_view']));
});