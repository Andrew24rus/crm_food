$( document ).ready(function() {

    pickmeup.defaults.locales['ru'] = {
        days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
        daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
    };

    pickmeup('.date', {
        format  : 'Y-m-d',
        locale : 'ru',
        default_date: false
    });

    $('table .show-desc').click(function() {
        var id = $(this).attr('data-id');
        var inst = $('.description-'+id).remodal();
        inst.open();
    });

    $('.mail').blur(function() {
        if($(this).val() != '') {
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if(pattern.test($(this).val())){

            } else {
                $(this).css({'border' : '4px solid red'});
                alert('Не верно указан email');
            }
        } else {
            $(this).css({'border' : '1px solid #CCCCCC'});
            // alert('Поле email не должно быть пустым');
        }
    });

    $('.mobile-phone').mask('+7-000-000-0000');

    $('.mobile-phone').on('change', function() {
        if (!$(this).hasClass("check-uniq")) {
            var value = '7' + $(this).cleanVal();
            console.log(value);
            var obj = this;
            $.ajax({
                url: '/api/check/mobil-phone/'+value,
                type: 'GET',
                success: function(data){
                    $('.uniq-message').remove();
                    $(data).insertAfter(obj);
                    if ( $(obj).siblings().hasClass('exist') ) {
                        $(obj).addClass('not-uniq');
                        $('form button').prop('disabled', true);
                    } else {
                        $(obj).removeClass('not-uniq');
                        $('form button').removeProp('disabled');
                    }
                }
            });
        }

    });


    $('.check-uniq').on('change', function() {
        var value = '7' + $(this).cleanVal();
        console.log(value);
        var obj = this;
        $.ajax({
            url: '/api/check/uniq-phone/'+value,
            type: 'GET',
            success: function(data){
                $('.uniq-message').remove();
                $(data).insertAfter(obj);
                if ( $(obj).siblings().hasClass('exist') ) {
                    $(obj).addClass('not-uniq');
                    $('form button').prop('disabled', true);
                } else {
                    $(obj).removeClass('not-uniq');
                    $('form button').removeProp('disabled');
                }
            }
        });
    });

    $('#autocomplete-client').autocomplete({
        serviceUrl: '/api/autocomplete/client',
        noCache: true,
        showNoSuggestionNotice: true,
        noSuggestionNotice: "Таких номеров в базе нет",
        onSelect: function (suggestion) {
            var client = suggestion.data;
            $('.name-client').val(client.name);
            $('.phone-mobile').val(client.phone_mobile);
            $('.phone-mobile').unmask();
            $('.phone-mobile').mask('+7-000-000-0000');
            $('.phone-mobile').trigger('change');
            $('.phone-mobile2').val(client.phone_mobile2);
            $('.phone-mobile2').unmask();
            $('.phone-mobile2').mask('+7-000-000-0000');
            $('.phone-mobile2').trigger('change');
            $('.client-id').val(client.id);
            $('select#company-select').val(client.company_id);
            $('select#company-select').trigger("chosen:updated");
            // console.log(suggestion);
        }
    });

    Flatpickr.localize(Flatpickr.l10ns.ru);

    $('.datetime').flatpickr({
        enableTime: true,
        time_24hr: true
    });


    $('.add-section a').on('click', function () {
        var name = '#' + $(this).attr('data-section');
        $(name).show('fast');
        $(this).hide('fast');
    });


    $('select#beznal').on('change', function() {
        if (this.value == 1) {
            $('#finance').show('fast');
            $('.sum-nam-beznal').show('fast');
        } else {
            $('#finance').hide('fast');
            $('.sum-nam-beznal').hide('fast');
        }
    });

    $('select#transport_owner').bind('change', function() {
        if (this.value == 1) {
            $('#our_transport').show('fast');
            $('#partner').hide('fast');
            $('#partner input').val('');
        }
        if (this.value == 2) {
            $('#partner').show('fast');
            $('#our_transport').hide('fast');
            $('#our_transport input').val('');
        }
        if (this.value == 3) {
            $('#partner').show('fast');
            $('#our_transport').show('fast');
        }
    });


    $('a#sum-partner').on('click', function () {
        var price_order = $(this).closest('section').find('.price_order').val();
        var beznal = $('#beznal').val();
        var proc = $(this).closest('section').find('.procent_partner').val();
        var sum_part = price_order / 100 * proc;
        var value = price_order - sum_part;
        $(this).parent().siblings('input').val(value);
    });

    $('a#sum-driver').on('click', function () {
        var price_order = $(this).closest('section').find('.price_order').val();
        // var price_order = $('#price_order').val();
        var value = price_order * 0.2; //20% от заказа
        $(this).parent().siblings('input').val(value);
    });
    $('a.calkulat').on('click', function () {
        $(this).closest('section').find('a#sum-partner').trigger('click');
        $(this).closest('section').find('a#sum-nam').trigger('click');
    });


    $('a#sum-nam').on('click', function () {
        var price_order = $(this).closest('section').find('.price_order').val();
        var beznal = $('#beznal').val();
        var proc = $(this).closest('section').find('.procent_partner').val();
        if (proc == 'Не указано') {
            alert('Не указан % партнера');
        }
        var sum_part = price_order / 100 * proc;
        var nalog = price_order / 100 * 7;
        if (beznal == 0) {
            var value = sum_part;
        } else {
            var value = price_order - nalog - (price_order - sum_part);
        }
        $(this).parent().siblings('input').val(value);
    });

    $('a.sum-nam-beznal').on('click', function () {
        var price_order = $(this).closest('section').find('.price_order').val();
        var sum_part = $(this).closest('section').find('[name ^= "order[summ_partner" ]').val();
        var itog = ( price_order * 0.93 ) - sum_part;
        $(this).parent().siblings('input').val(itog);
    });




    $('a.clean').on('click', function() {
        var select = $(this).closest('label').siblings('select');
        select.val('').trigger('chosen:updated');
    });

    $('#status').on('change', function() {
        $('select#status').trigger("chosen:updated");
        var status = $(this).val();
        // На Заявку
        if (status == 2) {
            var driver_id = $( "select[name='order[driver_id1]']" ).trigger('chosen:updated').val();
            var partner_id = $( "select[name='order[partner_id1]']" ).trigger('chosen:updated').val();
            if ( driver_id === '' && partner_id === '' ) {
                $('form button').prop('disabled', true);
                alert('Не заполнен ни водитель ни партнер');
                $('select#status').val(1).trigger('chosen:updated').trigger('change');
            } else {
                $('form button').removeProp('disabled');
            }
        }

        // Перезвонить или отмена
        if (status == 3 || status == 5) {
            var other_info = $('#other_info').val();
            if (other_info.length < 2) {
                alert('Не заполнено поле Прочая информация');
                $('form button').prop('disabled', true);
                $('select#status').val(1).trigger('chosen:updated').trigger('change');
            } else {
                $('form button').removeProp('disabled');
            }
        }

        // На выполнение
        if (status == 4) {
            var driver_id = $("select[name='order[driver_id1]']").trigger('chosen:updated').val();
            var driver_id2 = $("select[name='order[driver_id2]']").trigger('chosen:updated').val();
            var summ_zakaz_driver = $('#price_order1').val();
            var ocenka = $("select[name='order[rating_driver1]']").trigger('chosen:updated').val();
            var zp_driver = $("input[name='order[zp_driver1]']").val();

            var partner_id = $( "select[name='order[partner_id1]']" ).trigger('chosen:updated').val();
            var partner_id2 = $( "select[name='order[partner_id2]']" ).trigger('chosen:updated').val();
            var partner_id3 = $( "select[name='order[partner_id3]']" ).trigger('chosen:updated').val();

            var procent = $( "select[name='order[procent_partner1]']" ).trigger('chosen:updated').val();
            var summ_nam = $( "input[name='order[summ_nam1]']" ).val();
            var summ_partner = $( "input[name='order[summ_partner1]']" ).val();
            // alert(driver_id);

            if ( driver_id2.length !== 0 ) {
                var summ_zakaz_driver2 = $('#price_order2').val();
                var ocenka2 = $("select[name='order[rating_driver2]']").trigger('chosen:updated').val();
                var zp_driver2 = $("input[name='order[zp_driver2]']").val();
                if (summ_zakaz_driver2.length != 0 && ocenka2.length != 0  && (zp_driver2.length != 0 && zp_driver2 != '0' ) ) {
                    $('form button').removeProp('disabled');
                } else {
                    $('form button').prop('disabled', true);
                    alert('Не все поля заполнены у водителя или партнера1');
                    $('select#status').val(1).trigger('chosen:updated').trigger('change');
                }
            }

            if ( partner_id2.length !== 0 && partner_id2 !== 0) {
                console.log(partner_id2.length);
                console.log(partner_id2);
                var procent2 = $( "select[name='order[procent_partner2]']" ).trigger('chosen:updated').val();
                var summ_nam2 = $( "input[name='order[summ_nam2]']" ).val();
                var summ_partner2 = $( "input[name='order[summ_partner2]']" ).val();

                if (procent2.length != 0 && (summ_nam2.length != 0 && summ_nam2 != '0' )  &&   (summ_partner2.length != 0 && summ_partner2 != '0' )) {
                    $('form button').removeProp('disabled');
                } else {
                    console.log(procent2.length);
                    console.log(summ_nam2.length);
                    console.log(summ_nam2);
                    console.log(summ_partner2);
                    console.log(summ_partner2.length);
                    $('form button').prop('disabled', true);
                    alert('Не все поля заполнены у водителя или партнера2');
                    $('select#status').val(1).trigger('chosen:updated').trigger('change');
                }
            }


            if ( partner_id3.length !== 0 ) {
                var procent3 = $( "select[name='order[procent_partner3]']" ).trigger('chosen:updated').val();
                var summ_nam3 = $( "input[name='order[summ_nam3]']" ).val();
                var summ_partner3 = $( "input[name='order[summ_partner3]']" ).val();

                if (procent3.length != 0 && (summ_nam3.length != 0 && summ_nam3 != '0' )  &&   (summ_partner3.length != 0 && summ_partner3 != '0')) {
                    $('form button').removeProp('disabled');
                } else {
                    $('form button').prop('disabled', true);
                    alert('Не все поля заполнены у водителя или партнера');
                    $('select#status').val(1).trigger('chosen:updated').trigger('change');
                }
            }






            if ( (driver_id.length !== 0 && summ_zakaz_driver.length != 0 && ocenka.length != 0 && (zp_driver.length != 0 && zp_driver != '0' )) ||
                (partner_id.length != 0 && procent.length != 0 && (summ_nam.length != 0 && summ_nam != '0' )  &&   (summ_partner.length != 0 && summ_partner != '0' ) ) ) {
                $('form button').removeProp('disabled');
            } else {
                $('form button').prop('disabled', true);
                alert('Не все поля заполнены у водителя или партнера');
                $('select#status').val(1).trigger('chosen:updated').trigger('change');
            }



        }

        //Если устанавливаем на входящие
        if (status == 1) {
            $('form button').removeProp('disabled');
        }

    });

    $('input').click(function() {
        $(this).select();
    });
    $('textarea').click(function() {
        $(this).select();
    });

    $(document).on('keypress', '.num-only', function(e){          
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            if ($(this).siblings('.errmsg').length) {
                $(this).siblings('.errmsg').show().delay(3000).fadeOut(500);
            } else {
                $errmsg = '<span class="errmsg" style="color: red; margin-left: 15px;">Только цифры!</span>';
                $($errmsg).insertBefore(this).show().delay(3000).fadeOut(500);                
            }
            return false;
        }
    });

    $(document).on('paste', '.num-only', function(e) {
        var context = $(this);
        setTimeout(function() {
            context.val('');
        }, 0);
    });

});

    $( function() {
        $( ".datepicker" ).datepicker({ dateFormat: 'dd.mm.yy' });
    });

function AlertIt(id) {
    var answer = confirm ("Уверены что хотите скопировать заявку ?")
    if (answer)
        window.location="/admin/order/"+id+"/copy";
}