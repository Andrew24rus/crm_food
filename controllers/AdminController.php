<?php
use Illuminate\Database\Capsule\Manager as DB;

class AdminController extends Controller
{
    public function showUserList($request, $response, $args)
    {
        $users = User::with('role')->get()->toArray();
        return $this->ci->view->render($response, 'admin/users.html.twig', ['users' => $users]);
    }

    public function showUserRoles($request, $response, $args)
    {
        $roles = Role::all();
        return $this->ci->view->render($response, 'admin/user-roles.html.twig', ['roles' => $roles]);
    }    

    public function showUserEdit($request, $response, $args)
    {
        $roles = Role::orderBy('id', 'desc')->get();
        $user = User::with('role')->find($args['id']);
        return $this->ci->view->render($response, 'admin/users-add.html.twig', ['roles' => $roles, 'user' => $user]);
    }

    public function showUserAdd($request, $response, $args)
    {
        $roles = Role::orderBy('id', 'desc')->get();
        return $this->ci->view->render($response, 'admin/users-add.html.twig', ['roles' => $roles]);
    }

    public function showSettings($request, $response, $args)
    {
        return $this->ci->view->render($response, 'admin/settings/settings.html.twig');
    }
/*
    public function checkUniq($request, $response, $args)
    {
        $data = DB::table($args['table'])->where($args['field'], $args['value'])->first();
        if ($data) {
            $message = '<div class="uniq-message">Такое значение уже есть у <b>'.$data->name.'</b></div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
        $message = '<div class="uniq-message">Такого значения нет</div>';
        return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
    }

    public function checkUniqPhone($request, $response, $args)
    {
        $count = strlen($args['value']);
        if ($count < 11 && $count > 1) {
            $message = '<div class="uniq-message exist">Недостаточно цифр в номере</div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
        $data = DB::table('clients')->where('phone_mobile', $args['value'])
            ->orWhere('phone_mobile2', $args['value'])
            ->orWhere('phone_dop', $args['value'])
            ->first();
        if ($data) {
            $message = '<div class="uniq-message exist">Такое значение уже есть у <a target="_blank" href="/admin/client/'.$data->id.'">'.$data->name.'</a></div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
        $message = '<div class="uniq-message">Такого значения нет</div>';
        return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
    }

    public function checkMobilePhone($request, $response, $args)
    {
        $count = strlen($args['value']);
        if ($count < 11 && $count > 1 ) {
            $message = '<div class="uniq-message exist">Недостаточно цифр в номере</div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        } else {
            $message = '<div class="uniq-message"></div>';
            return $this->ci->view->render($response, 'ajax/message.html.twig', ['message' => $message]);
        }
    }
*/
    public function showIndex()
    {
        $this->slim->redirect('/admin/content');
    }
}