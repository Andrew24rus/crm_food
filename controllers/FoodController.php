<?php
use Illuminate\Database\Capsule\Manager as DB;

class FoodController extends Controller
{
    public function showFood($request, $response, $args)
    {
        $this->twig_vars['food'] = json_encode(Food::all()->toArray());
        //ddd($this->twig_vars);
        $this->view = 'food.html.twig';
        return $this->render();
    }

    public function createFood($request, $response, $args) {
        $food = $this->data;

        if (isset($food['id']) && $food['id']) {
            $f = Food::find($food['id']);
            if ($f) {
                $f->update($food);
                echo json_encode(Food::all()->toArray());
                die;
            }
            unset($food['id']);
        }
        Food::create($food);
        echo json_encode(Food::all()->toArray());
    }

    public function deleteFood($request, $response, $args) {
        $id = $this->data['id'];
        Food::destroy($id);
    }
}