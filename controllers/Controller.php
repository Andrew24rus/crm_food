<?php

class Controller
{
    public $slim;
    public $twig_vars;
    public $view;
    protected $ci;
    protected $data;

    public function __construct($container) {
        $this->ci = $container;
        $request = $this->ci->get('request');
        $this->data = $request->isGet() ? $request->getQueryParams() : $request->getParsedBody();
    }

    public function render() {
    	$response = $this->ci->get('response');
        return $this->ci->view->render($response, $this->view, $this->twig_vars);
    }
}