<?php

class Model
{
    public $id;

    public function __construct($data)
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        }
    }
    
    public function save()
    {
        if (strlen($this->id) > 0 ) {
            $record = ORM::for_table($this::table)->find_one($this->id);
        } else {
            $record = ORM::for_table($this::table)->create();

        }
        foreach ($this as $key => $value) {
            $record->$key = $value;
        }
        $record->save();
        // ddd($record->id());
        $this->id = $record->id;
        // ddd($this);
    }

    public static function load($id) {
        $record = ORM::for_table(static::table)->find_one($id);
        return $record;
    }

    public static function delete($id) {
        $record = ORM::for_table(static::table)->find_one($id);
        $record->delete();
    }
}