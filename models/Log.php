<?php

class Log
{
    
    const TABLE = 'log';

    public static function write($message, $entity = 'system', $entity_id = 0)
    {
        $record = ORM::for_table(self::TABLE)->create();
        $record->date = date('Y-m-d H:i:s');
        $record->message = $message;
        $record->entity = $entity;
        $record->entity_id = $entity_id;
        $record->save();
    }
}