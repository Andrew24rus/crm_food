<?php

class Food extends Illuminate\Database\Eloquent\Model
{
	protected $table = 'food';
    public $timestamps = true;
    protected $guarded = [];

}